# Agenda Roulette

For those status meetings where you just go around the group one by one for updates.

Keep things 'interesting' by not knowing what comes next!

Populate a local `agenda.json` with an array of agenda items/people:

```
{
  "items":["Mary","Bob","Carl","Jane","Dana"]
}
```
