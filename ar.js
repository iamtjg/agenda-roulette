'use strict';

var agenda = {};

/** Get agena info */
var getA = new XMLHttpRequest();
getA.addEventListener('load', haveA);
getA.open('GET', 'agenda.json');
getA.overrideMimeType('application/json'); // this is likely to be run on a local file system and may receive malformed JSON warnings/errors otherwise
getA.send();

document.getElementById('next').addEventListener('click', function(event) {

  document.getElementById('hotseat').textContent = popItem();

});

function getRandomIntInclusive(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/** Returns random item from agenda and removes then removes it  */
function popItem() {
  if (agenda.length != 0) {
    let rand = getRandomIntInclusive(0, agenda.length - 1);
    let item = agenda[rand];
    agenda.splice(rand, 1);
    return item;
  }
  document.getElementById('next').classList.add('hide');
  return 'Get back to work!';
}

/** Populates agenda with data and displays controls */
function haveA(e) {
  agenda = JSON.parse(this.response).items;
  document.getElementById('ar').classList.remove('hide');
  document.getElementById('ar').classList.add('show');
}
